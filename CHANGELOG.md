# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.10.0

- minor: Upgrade pipe's base docker image to alpine:3.20.
- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.9.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.8.1

- patch: Internal maintainance: Refactor tests.

## 0.8.0

- minor: Update alpine docker image and packages in Dockerfile.
- patch: Internal maintenance: update pipes versions in pipelines config file.

## 0.7.1

- patch: Update a link to the guide for multiple SSH keys usage.

## 0.7.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.6.0

- minor: Internal maintenance: Update bitbucket-pipes-toolkit to fix vulnerability with certify CVE-2023-37920.

## 0.5.12

- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Bump version of the pipe's dependencies.

## 0.5.11

- patch: Add an example with both alternative port for ssh and sftp.

## 0.5.10

- patch: Internal maintenance: Added missed files to fit standart of pipe structure.

## 0.5.9

- patch: Internal maintenance: Bump versions of bitbucket_pipes_toolkit to 3.2.1 requests to 2.26.* awscli to 1.23.* boto3 to 1.22.* docker[tls] to 4.4.* pytest to 6.*

## 0.5.8

- patch: Internal maintenance: Add required tags for test infra resources.
- patch: Internal maintenance: Fix package versions in test requirements.

## 0.5.7

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.5.6

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.5.5

- patch: Internal maintenance: Add gitignore secrets.

## 0.5.4

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.5.3

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.5.2

- patch: Internal maintenance: Refactor tests to python.

## 0.5.1

- patch: Internal maintenance: Automate test infrastructure.

## 0.5.0

- minor: Add variable PASSWORD as an alternative authentication method.

## 0.4.1

- patch: Update pipes bash toolkit version.

## 0.4.0

- minor: LOCAL_PATH had become an optional parameter

## 0.3.1

- patch: Standardising README and pipes.yml.

## 0.3.0

- minor: Force the git ssh command used when pushing a new tag to override the default SSH configuration for piplines.

## 0.2.0

- minor: Adopt new naming and consistency conventions.

## 0.1.3

- patch: Fix use quotes for all pipes examples in README.md.

## 0.1.2

- patch: Use quotes for all pipes examples in README.md.

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines SFTP pipe.

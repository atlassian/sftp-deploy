FROM alpine:3.20

RUN apk --no-cache add \
    openssh~=9 \
    sshpass~=1 \
    bash~=5 && \
    wget --no-verbose -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh && \
    rm -rf /var/lib/apt/lists/*

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]
